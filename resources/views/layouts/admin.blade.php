<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> .:: CARWASH ::.</title>

    {!!Html::style('css/bootstrap.min.css')!!}
    {!!Html::style('css/metisMenu.min.css')!!}
    {!!Html::style('css/sb-admin-2.css')!!}
    {!!Html::style('css/font-awesome.min.css')!!}


    <!--<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/metisMenu.min.css" rel="stylesheet">
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">-->

</head>

<body>

    <div id="wrapper">


        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand"><i class="fa fa-car"></i> CAR - WASH </a>
            </div>


        <ul class="nav navbar-top-links navbar-right">
                 <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              {!!Auth::user()->name!!}<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Ajustes</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{!!URL::to('/')!!}"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                     @if(Auth::user()->id == 1)
                        <li>
                            <a href="{!!URL::to('Usuario')!!}"><i class="fa fa-users fa-fw"></i> Usuario<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!!URL::to('Usuario/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('Usuario')!!}"><i class='fa fa-list-ol fa-fw'></i> Usuarios</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    {{--
                    <li>
                            <a href="#"><i class="fa fa-child"></i> Cliente<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!!URL::to('Cliente/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('Cliente')!!}"><i class='fa fa-list-ol fa-fw'></i> Clientes</a>
                                </li>
                            </ul>
                        </li> -->

                    --}}

                        <li>
                            <a href="#"><i class="fa fa-car"></i> Vehiculo<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!!URL::to('Vehiculo/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('Vehiculo')!!}"><i class='fa fa-list-ol fa-fw'></i> Vehiculos</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-cloud"></i> Lavado<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!!URL::to('Lavado/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('Lavado')!!}"><i class='fa fa-list-ol fa-fw'></i> Lavados</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="glyphicon glyphicon-list-alt"></i> Comprobante<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!!URL::to('Comprobante/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('Comprobante')!!}"><i class='fa fa-list-ol fa-fw'></i> Comprobantes</a>
                                </li>
                            </ul>
                        </li>



                        <li>
                            <a href="#"><i class="glyphicon glyphicon-book"></i> Consultas <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!!URL::to('MejorLavador')!!}"><i class='glyphicon glyphicon-star'></i> Mejor Lavador</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('MejorAuto')!!}"><i class='fa fa-cab'></i> Auto mas Lavado</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('Buscar')!!}"><i class='fa fa-money'></i> Recaudacion del dia</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('admin')!!}"><i class='fa fa-caret-square-o-left'></i> Regresar</a>
                                </li>
                            </ul>
                        </li>

                       <!-- <li>
                            <a href="#"><i class="glyphicon glyphicon-book"></i> Reporte 2<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!!URL::to('MejorAuto')!!}"><i class='fa fa-cab'></i> Auto mas Lavado</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('admin')!!}"><i class='fa fa-caret-square-o-left'></i> Regresar</a>
                                </li>
                            </ul>
                        </li>


                        <li>
                            <a href="#"><i class="glyphicon glyphicon-book"></i> Reporte 3<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!!URL::to('Buscar')!!}"><i class='fa fa-money'></i> Recaudacion del dia</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('admin')!!}"><i class='fa fa-caret-square-o-left'></i> Regresar</a>
                                </li>
                            </ul>
                        </li>
                       -->


                      <li>
                            <a href="#"><i class="fa fa-file-o"></i> Reportes PDF - EXCEL <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#"><i class='glyphicon glyphicon-star'></i> Mejor Lavador</a>
                                </li>
                                <li>
                                    <a href="#"><i class='fa fa-cab'></i> Auto mas Lavado</a>
                                </li>
                               	<li>
                                    <a href="#"><i class='fa fa-caret-square-o-left'></i> Regresar</a>
                                </li>
                            </ul>
                        </li>








                    </ul>
                </div>


            </div>



     </nav>

        <div id="page-wrapper">
            @yield('content')
        </div>

    </div>


    {!!Html::script('js/jquery.min.js')!!}
    {!!Html::script('js/bootstrap.min.js')!!}
    {!!Html::script('js/metisMenu.min.js')!!}
    {!!Html::script('js/sb-admin-2.js')!!}


    <!--<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metisMenu.min.js"></script>
    <script src="js/sb-admin-2.js"></script>-->

</body>

</html>
