	<div class="form-group">
		{!!Form::label('nrocomprobante','Comprobante:')!!}
		{!!Form::text('nrocomprobante',null,['class'=>'form-control','placeholder'=>'Ingresa el  Numero de Comprobante', 'autocomplete'=>'off'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('tipocomprobante','Tipo:')!!}
		{!!Form::select('tipocomprobante',array('Factura' => 'Factura', 'Boleta' => 'Boleta'),null,['class'=>'form-control'])!!}
	</div>	


	<div class="form-group">
		{!!Form::label('hora','Hora:')!!}
		{!!Form::text('hora',null,['class'=>'form-control','placeholder'=>'Ingresa la Hora', 'autocomplete'=>'off'])!!}
	</div>
	<div class="form-group">
		{!!Form::label('fecha','Fecha:')!!}
		{!!Form::date('fecha',null,['class'=>'form-control', 'autocomplete'=>'off'])!!}
	</div>
 	
 	<div class="form-group">
		{!!Form::label('costototal','Costo:')!!}
		{!!Form::text('costototal',null,['class'=>'form-control','placeholder'=>'Ingresa el  Costo Total', 'autocomplete'=>'off'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('id_usuario','Usuario:')!!}
		{!!Form::select('id_usuario',$usuario,array(),['class'=>'form-control'])!!}
	</div>
	
	<div class="form-group">
		{!!Form::label('id_lavado','Lavado:')!!}
		{!!Form::select('id_lavado',$lavado,array(),['class'=>'form-control'])!!}
	</div>