@extends('layouts.admin')
@section('content')
@include('alerts.request')

	<br>
	<div>
		{!!Form::model($comprobantes,['route'=>['Comprobante.update',$comprobantes],'method'=>'PUT'])!!}	
			@include('comprobante.forms.com')
	</div>
	<div class="cuadro">
	<table style="  border-collapse: separate;
 	 border-spacing: 10px 5px">
		
		<td>
			{!!Form::submit('Actualizar',['class'=>'btn btn-info'])!!}
			{!!Form::close()!!}
		</td>
		<td>
		<a href="{{URL::to('/Comprobante')}}" class="btn btn-success">Regresar</a>
		</td>
		<!--<td>
			{!!Form::open(['route'=>['Comprobante.destroy', $comprobantes], 'method' => 'DELETE'])!!}
			{!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
			{!!Form::close()!!}

		</td>-->
	</table>
	@endsection	