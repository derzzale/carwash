@extends('layouts.admin')

@include('alerts.success')

	@section('content')
	<table class="table">
		<thead>
			<th>ID</th>
			<th>COMPROBANTE</th>
			<th>TIPO</th>
			<th>HORA</th>
			<th>FECHA</th>
			<th>COSTO</th>
			<th>ID USUARIO</th>
			<th>ID LAVADO</th>
			<!--<th>VER</th>-->
			<th>EDITAR</th>
			<th>ELIMINAR</th>

		</thead>
		@foreach($comprobantes as $comprobante)
			<tbody>
				<td>{{$comprobante->id}}</td>
				<td>{{$comprobante->nrocomprobante}}</td>
				<td>{{$comprobante->tipocomprobante}}</td>
				<td>{{$comprobante->hora}}</td>
				<td>{{$comprobante->fecha}}</td>
				<td>{{$comprobante->costototal}}</td>
				<td>{{$comprobante->id_usuario}}</td>
				<td>{{$comprobante->id_lavado}}</td>
				<!--<td>
			{!!link_to_route('Comprobante.show', $title = 'VER', $parameters = $comprobante->id, $attributes = ['class'=>'btn btn-info'])!!}
				</td>-->
				<td>
				{!!link_to_route('Comprobante.edit', $title = 'OPCION', $parameters = $comprobante->id, $attributes = ['class'=>'btn btn-primary'])!!}
				</td>
				<td>
			{!!Form::open(['route'=>['Comprobante.destroy', $comprobante], 'method' => 'DELETE'])!!}
			{!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
			{!!Form::close()!!}
				</td>
			</tbody>
		@endforeach
	</table>
	{!!$comprobantes->render()!!}
	
	@endsection

