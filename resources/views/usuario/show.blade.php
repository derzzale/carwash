@extends('layouts.admin')


@include('alerts.success')



	@section('content')
	<table class="table">
		<thead>
			<th>ID</th>
			<th>DNI</th>	
			<th>NOMBRES</th>
			<th>APELLIDOS</th>
			<th>EMAIL</th>
			<th>ESTATUS</th>
			<th>TIPO</th>
			<th>VER</th>
			<th>EDITAR</th>
			<th>ELIMINAR</th>
		</thead>
	@foreach($users as $user)
			<tbody>
				<td>{{$user->id}}</td>
				<td>{{$user->dni}}</td>
				<td>{{$user->name}}</td>
				<td>{{$user->apellidos}}</td>
				<td>{{$user->email}}</td>
				<td>{{$user->estatus}}</td>
				<td>{{$user->tipo}}</td>
				<td>
			</tbody>
		@endforeach
	</table>
		{!!$users->render()!!}
	@endsection