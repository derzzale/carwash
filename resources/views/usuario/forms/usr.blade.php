	<div class="form-group">
		{!!Form::label('dni','Dni:')!!}
		{!!Form::text('dni',null,['class'=>'form-control','placeholder'=>'Ingresa el Dni del usuario', 'autocomplete'=>'off']) !!}
	</div>
	<div class="form-group">
		{!!Form::label('name','Nombre:')!!}
		{!!Form::text('name',null,['class'=>'form-control','placeholder'=>'Ingresa el Nombre del usuario', 'autocomplete'=>'off']) !!}
	</div>

	<div class="form-group">
		{!!Form::label('apellidos','Apellidos:')!!}
		{!!Form::text('apellidos',null,['class'=>'form-control','placeholder'=>'Ingresa el Apellido del usuario', 'autocomplete'=>'off'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('email','Correo:')!!}
		{!!Form::text('email',null,['class'=>'form-control','placeholder'=>'Ingresa el Nombre del usuario', 'autocomplete'=>'off'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('password','Contraseña:')!!}
		{!!Form::password('password',['class'=>'form-control','placeholder'=>'Ingresa la Contraseña del usuario', 'autocomplete'=>'off'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('estatus','Estatus:')!!}
		{!!Form::select('estatus',[''=>'Seleccione un nivel..','Admnistrador'=> 'Administrador','Cajero'=> 'Cajero','Lavador'=> 'Lavador'], null ,['class'=>'form-control'])!!}
	</div>


	<div class="form-group">
		{!!Form::label('tipo','Tipo:')!!}
		{!!Form::select('tipo',[''=>'Seleccione un nivel..','Activo'=> 'Activo','Inactivo'=> 'Inactivo'], null ,['class'=>'form-control'])!!}
	</div>