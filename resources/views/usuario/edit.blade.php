@extends('layouts.admin')
@section('content')
@include('alerts.request')
		
	<br>
	<div>
		{!!Form::model($user,['route'=>['Usuario.update',$user],'method'=>'PUT'])!!}
			@include('usuario.forms.usr')
	</div>
	<div class="cuadro">
	<table style="  border-collapse: separate;
 	 border-spacing: 10px 5px">
		
		<td>
			{!!Form::submit('Actualizar',['class'=>'btn btn-info'])!!}
			{!!Form::close()!!}
		</td>
		
		<td>
		<a href="{{URL::to('/Usuario')}}" class="btn btn-success">Regresar</a>
		</td>
		<!--<td>
			{!!Form::open(['route'=>['Usuario.destroy', $user], 'method' => 'DELETE'])!!}
			{!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
			{!!Form::close()!!}

		</td>-->
	</table>
	@endsection