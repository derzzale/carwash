@extends('layouts.admin')
	@section('content')
	@if(count($errors) > 0)
		<div class="alert alert-danger alert-dismissible" role="alert">
	  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  	<ul>
	  		@foreach($errors->all() as $error)
	  		<li>{!! $error !!}</li>
	  		@endforeach
	  	</ul>
		</div>
	@endif
	
		<div>
	{!!Form::open(['route'=>'Vehiculo.store', 'method'=>'POST'])!!}
			@include('vehiculo.forms.veh')
	</div>
	
	<div class="cuadro">
	<table style="  border-collapse: separate; border-spacing: 10px 5px">
		<td>
	{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
	{!!Form::close()!!}
		</td>
		<td>
			<a href="{{URL::to('/Vehiculo')}}" class="btn btn-success">Regresar</a>
		</td>
	<table>
		


	@endsection