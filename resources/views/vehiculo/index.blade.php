@extends('layouts.admin')


@include('alerts.success')

	@section('content')
	<table class="table">
		<thead>
			<th>ID</th>
			<th>PROPIEATRIO</th>
			<th>PLACA</th>
			<th>MARCA</th>
			<th>MODELO</th>
			<th>COLOR</th>
			<!--<th>VER</th>-->
			<th>EDITAR</th>
			<th>ELIMINAR</th>
		</thead>
	@foreach($vehiculos as $vehiculo)
			<tbody>
				<td>{{$vehiculo->id}}</td>
				<td>{{$vehiculo->propietario}}</td>
				<td>{{$vehiculo->placa}}</td>
				<td>{{$vehiculo->marca}}</td>
				<td>{{$vehiculo->modelo}}</td>
				<td>{{$vehiculo->color}}</td>
				<!--<td>
			{!!link_to_route('Vehiculo.show', $title = 'VER', $parameters = $vehiculo->id, $attributes = ['class'=>'btn btn-info'])!!}
				</td>-->
				<td>
			{!!link_to_route('Vehiculo.edit', $title = 'EDITAR', $parameters = $vehiculo->id, $attributes = ['class'=>'btn btn-primary'])!!}
				</td>
				
				<td>
			{!!Form::open(['route'=>['Vehiculo.destroy', $vehiculo], 'method' => 'DELETE'])!!}
			{!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
			{!!Form::close()!!}

				</td>
			</tbody>
		@endforeach
	</table>
		{!!$vehiculos->render()!!}
	@endsection