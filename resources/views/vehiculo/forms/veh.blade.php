	<div class="form-group">
		{!!Form::label('propietario','Propietario:')!!}
		{!!Form::text('propietario',null,['class'=>'form-control','placeholder'=>'Ingresa la Nombre de Propietario del Vehiculo', 'autocomplete'=>'off']) !!}
	</div>

	<div class="form-group">
		{!!Form::label('placa','Placa:')!!}
		{!!Form::text('placa',null,['class'=>'form-control','placeholder'=>'Ingresa la Placa del Vehiculo', 'autocomplete'=>'off']) !!}
	</div>

	<div class="form-group">
		{!!Form::label('marca','Marca:')!!}
		{!!Form::text('marca',null,['class'=>'form-control','placeholder'=>'Ingresa la Marca del Vehiculo', 'autocomplete'=>'off'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('modelo','Modelo:')!!}
		{!!Form::text('modelo',null,['class'=>'form-control','placeholder'=>'Ingresa el Modelo del Vehiculo', 'autocomplete'=>'off'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('color','Color:')!!}
		{!!Form::text('color',null,['class'=>'form-control','placeholder'=>'Ingresa el Color del Vehiculo', 'autocomplete'=>'off'])!!}
	</div>