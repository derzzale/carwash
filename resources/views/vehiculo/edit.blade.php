@extends('layouts.admin')
@section('content')
@include('alerts.request')
		
	<br>
	<div>
		{!!Form::model($vehiculo,['route'=>['Vehiculo.update',$vehiculo],'method'=>'PUT'])!!}
			@include('vehiculo.forms.veh')
	</div>
	<div class="cuadro">
	<table style="  border-collapse: separate;
 	 border-spacing: 10px 5px">
		
		<td>
			{!!Form::submit('Actualizar',['class'=>'btn btn-info'])!!}
			{!!Form::close()!!}
		</td>
		<td>
		<a href="{{URL::to('/Vehiculo')}}" class="btn btn-success">Regresar</a>
		</td>
		<!--<td>
			{!!Form::open(['route'=>['Vehiculo.destroy', $vehiculo], 'method' => 'DELETE'])!!}
			{!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
			{!!Form::close()!!}

		</td>-->
	</table>
	@endsection