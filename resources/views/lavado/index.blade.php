@extends('layouts.admin')

@include('alerts.success')

	@section('content')
	<table class="table">
		<thead>
			<th>ID</th>
			<th>TIPO</th>
			<th>COSTO</th>
			<th>FECHA INGRESO</th>
			<th>FECHA SALIDA</th>
			<th>TERMINADO</th>
			<th>LAVADOR</th>
			<th>ID VEHICULO</th>
			<!--<th>VER</th>-->
			<th>EDITAR</th>
			<th>ELIMINAR</th>

		</thead>
		@foreach($lavados as $lavado)
			<tbody>
				<td>{{$lavado->id}}</td>
				<td>{{$lavado->tipo}}</td>
				<td>{{$lavado->costo}}</td>
				<td>{{$lavado->fechaingreso}}</td>
				<td>{{$lavado->fechasalida}}</td>
				<td>{{$lavado->culminacion}}</td>
				<td>{{$lavado->user_id}}</td>
				<td>{{$lavado->vehiculo_id}}</td>
				{{-- 				<td>{{$lavado->lavadousername}}</td>
								<td>{{$lavado->lavadoplacavehiculo}}</td> --}}

				<!--<th>
				{!!link_to_route('Lavado.show', $title = 'VER', $parameters = $lavado->id, $attributes = ['class'=>'btn btn-info'])!!}
				</th>-->
				<th>
				{!!link_to_route('Lavado.edit', $title = 'EDITAR', $parameters = $lavado->id, $attributes = ['class'=>'btn btn-primary'])!!}
				</th>
				<td>
					{!!Form::open(['route'=>['Lavado.destroy', $lavado], 'method' => 'DELETE'])!!}
					{!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
					{!!Form::close()!!}

				</td>
			</tbody>
		@endforeach
	</table>
	{!!$lavados->render()!!}

	@endsection
