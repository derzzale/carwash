@extends('layouts.admin')
@section('content')
@include('alerts.request')

	<br>
	<div>
		{!!Form::model($lavados,['route'=>['Lavado.update',$lavados],'method'=>'PUT'])!!}	
			@include('lavado.forms.lav')
	</div>
	<div class="cuadro">
	<table style="  border-collapse: separate;
 	 border-spacing: 10px 5px">
		
		<td>
			{!!Form::submit('Actualizar',['class'=>'btn btn-info'])!!}
			{!!Form::close()!!}
		</td>
		<td>
		<a href="{{URL::to('/Lavado')}}" class="btn btn-success">Regresar</a>
		</td>
		
		<!--td>
			{!!Form::open(['route'=>['Lavado.destroy', $lavados], 'method' => 'DELETE'])!!}
			{!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
			{!!Form::close()!!}

		</td>-->
	</table>
	@endsection	