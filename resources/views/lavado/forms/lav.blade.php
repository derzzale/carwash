	
	<div class="form-group">
		{!!Form::label('tipo','Tipo:')!!}
		{!!Form::select('tipo',array('Aspiradora' => 'Aspiradora', 'Pulidora' => 'Pulidora','Manguera' => 'Manguera'),null,['class'=>'form-control'])!!}
	</div>	

	<div class="form-group">
		{!!Form::label('costo','Costo:')!!}
		{!!Form::text('costo',null,['class'=>'form-control','placeholder'=>'Ingresa Costo del Lavado', 'autocomplete'=>'off'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('fechaingreso','Fecha Ingreso:')!!}
		{!!Form::date('fechaingreso',null,['class'=>'form-control'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('fechasalida','Fecha Salida:')!!}
		{!!Form::date('fechasalida',null,['class'=>'form-control'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('culminacion','Terminado')!!}
		{!!Form::select('culminacion',array('Si' => 'Si', 'No' => 'No'),null,['class'=>'form-control'])!!}
	</div>	

	<div class="form-group">
		{!!Form::label('id_usuario','Usuario:')!!}
		{!!Form::select('id_usuario',$usuario,array(),['class'=>'form-control'])!!}
	</div>
	
	<div class="form-group">
		{!!Form::label('id_vehiculo','Vehiculo:')!!}
		{!!Form::select('id_vehiculo',$vehiculo,array(),['class'=>'form-control'])!!}
	</div>