@extends('layouts.admin')
	@include('alerts.errors')
	@include('alerts.request')
@section('content')
	
	<div style=" margin:0px 0px 0px 0px; width: 100%; height: 600px; background-color:#BDBDBD; display: flex;
  justify-content: center; align-items: center;">
		<div class="clearfix"></div>
			

			<div style="background-color: #F2F2F2;padding: 10px 10px 10px 10px;">
			<br>
				{!!Form::open(['route'=>'Buscar.store', 'method'=>'POST'])!!}
					<div class="form-group">
						{!!Form::label('fecha','Ingrese la Fecha:')!!}	
						{!!Form::date('fecha',null,['class'=>'form-control', 'placeholder'=>'Ingrese la Fecha'])!!}
					</div>
					{!!Form::submit('Determinar',['class'=>'btn btn-primary'])!!}
				{!!Form::close()!!}
			</div>
	</div>
		
	@stop