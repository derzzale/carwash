	<div class="form-group">
		{!!Form::label('Dni:')!!}
		{!!Form::text('dni',null,['class'=>'form-control','placeholder'=>'Ingresa el Dni del Cliente'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('Nombre:')!!}
		{!!Form::text('nombres',null,['class'=>'form-control','placeholder'=>'Ingresa el Nombre del Cliente'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('Apellidos:')!!}
		{!!Form::text('apellidos',null,['class'=>'form-control','placeholder'=>'Ingresa el Apellido del Cliente'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('sexo','Sexo:')!!}
		{!!Form::select('sexo',['Masculino'=> 'Masculino','Femenino'=> 'Femenino'], null ,['class'=>'form-control'])!!}
	</div>

	<div class="form-group">
		{!!Form::label('Telefono:')!!}
		{!!Form::text('telefono',null,['class'=>'form-control','placeholder'=>'Ingresa el Telefono del Cliente'])!!}
	</div>
