@extends('layouts.admin')
@section('content')
@include('alerts.request')	
	<br>
	<div>
		{!!Form::model($cliente,['route'=>['Cliente.update',$cliente],'method'=>'PUT'])!!}
			@include('cliente.forms.cl')
	</div>
	<div class="cuadro">
	<table style="  border-collapse: separate; border-spacing: 10px 5px">
		<td>
			{!!Form::submit('Aceptar',['class'=>'btn btn-primary'])!!}
			{!!Form::close()!!}
		</td>
		
		<td>
		<a href="{{URL::to('/Cliente')}}" class="btn btn-success">Regresar</a>
		</td>

<!--		<td>
			{!!Form::open(['route'=>['Cliente.destroy', $cliente], 'method' => 'DELETE'])!!}
			{!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
			{!!Form::close()!!}

		</td>	-->
	</table>
	@endsection