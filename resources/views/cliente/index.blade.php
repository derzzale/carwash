@extends('layouts.admin')

@include('alerts.success')

	@section('content')
	<table class="table">
		<thead>
			<th>ID</th>
			<th>Dni</th>
			<th>Nombres</th>
			<th>Email</th>
			<th>Sexo</th>
			<th>Telefono</th>
			<!--<th>Ver</th>-->
			<th>EDITAR</th>
			<th>ELIMINAR</th>
		</thead>
		@foreach($clientes as $cliente)
			<tbody>
				<td>{{$cliente->id}}</td>
				<td>{{$cliente->dni}}</td>
				<td>{{$cliente->nombres}}</td>
				<td>{{$cliente->apellidos}}</td>
				<td>{{$cliente->sexo}}</td>
				<td>{{$cliente->telefono}}</td>
				<!--<td>
				{{ link_to('cliente'.$cliente->id, 'Ver') }}
				{!!link_to_route('Cliente.show', $title = 'Ver', $parameters = $cliente->id, $attributes = ['class'=>'btn btn-info'])!!}
				<a href="cliente/show/{{ $cliente->id}}" class="btn btn-info"> Ver</a>
				</td>-->
				<td>
				{!!link_to_route('Cliente.edit', $title = 'Editar', $parameters = $cliente->id, $attributes = ['class'=>'btn btn-primary'])!!}

				</td>
				<td>
					{!!Form::open(['route'=>['Cliente.destroy', $cliente], 'method' => 'DELETE'])!!}
					{!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
				</td>
			</tbody>
		@endforeach
	</table>
	{!!$clientes->render()!!}
	@endsection