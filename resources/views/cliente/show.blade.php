@extends('layouts.admin')
	@section('content')
	
	<table class="table">
		<thead>
			<th>ID</th>
			<th>Dni</th>
			<th>Nombres</th>
			<th>Email</th>
			<th>Sexo</th>
			<th>Telefono</th>
		</thead>
		@foreach($clientes as $cliente)
			<tbody>
				<td>{{$cliente->id}}</td>
				<td>{{$cliente->dni}}</td>
				<td>{{$cliente->nombres}}</td>
				<td>{{$cliente->apellidos}}</td>
				<td>{{$cliente->sexo}}</td>
				<td>{{$cliente->telefono}}</td>
				<td>
                    <a href="{{URL::to('/Cliente')}}" class="btn btn-default">Regresar</a>
				</td>
				
			</tbody>
		@endforeach
	</table>

	@endsection