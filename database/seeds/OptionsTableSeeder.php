<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'dni' => '48382757',
                'name' => 'Roel Irvin',
                'apellidos' => 'Velarde Estrada',
                'email' => 'irvinvelarde@gmail.com',
                'password' => bcrypt('123'),
                'estatus' => 'Administrador',
                'tipo'  => 'activo'
            ]
        ]);
    }
}
