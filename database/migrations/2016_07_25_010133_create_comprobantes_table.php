<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comprobantes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nrocomprobante');
			$table->string('tipocomprobante');
			$table->string('hora');
			$table->date('fecha');
			$table->integer('costototal');

			$table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('users');

            $table->integer('id_lavado')->unsigned();
            $table->foreign('id_lavado')->references('id')->on('lavados');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comprobantes');
	}

}
