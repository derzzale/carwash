<?php namespace CarWash\Http\Requests;

use CarWash\Http\Requests\Request;

class LavadoUpdateRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'tipo' => 'required',
            'costo' => 'required',
            'fechaingreso' => 'required',
            'fechasalida' => 'required',
            'culminacion' => 'required',
            'id_usuario' => 'required',
            'id_vehiculo' => 'required',
		];
	}

}
