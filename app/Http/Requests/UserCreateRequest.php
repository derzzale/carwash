<?php namespace CarWash\Http\Requests;

use CarWash\Http\Requests\Request;

class UserCreateRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'dni' => 'required',
			'name' => 'required',
            'apellidos' => 'required',
            'email' => 'required',
            'password' => 'required',
            'estatus' => 'required',
            'tipo' => 'required'
		];
	}

}
