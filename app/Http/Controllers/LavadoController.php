<?php namespace CarWash\Http\Controllers;

use CarWash\Http\Requests;
use CarWash\Http\Requests\LavadoCreateRequest;
use CarWash\Http\Requests\LavadoUpdateRequest;
use CarWash\Http\Controllers\Controller;
use CarWash\Lavado;
use Session;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use CarWash\Vehiculo;
Use CarWash\User;

class LavadoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index(){
		/*$lavados = Lavado::select('lavados.id',
		'lavados.tipo',
		'lavados.costo',
		'lavados.fechaingreso',
		'lavados.fechasalida',
		'lavados.fechaingreso',
		'lavados.culminacion',
		'users.name as lavadousername',
		'vehiculos.placa as lavadoplacavehiculo')
		->join('users','users.id','=','lavados.id_usuario')
		->join('vehiculos','vehiculos.id','=','lavados.id_vehiculo')
		->paginate(5);
		return view('lavado.index',compact('lavados')); */
		
				$lavados = Lavado::all();
							//envia infrmacion con compact
        //return view('lavado.index',compact('lavados'));
		dd($lavados);
			  /// var_dump($lavados);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
		$vehiculo = Vehiculo::lists('placa','id')->prepend('Seleccione Placa del Vehiculo');
    $usuario = User::lists('name','id')->prepend('Seleccione el Nombre del Usuario');
        return view('lavado.create',compact('vehiculo','usuario'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(LavadoCreateRequest $request){
		    Lavado::create([
            'tipo' => $request['tipo'],
            'costo' => $request['costo'],
            'fechaingreso' => $request['fechaingreso'],
            'fechasalida' => $request['fechasalida'],
            'culminacion' => $request['culminacion'],
            'user_id' => $request['id_usuario'],
            'vehiculo_id' => $request['id_vehiculo'],
        ]);
        Session::flash('message','Lavado Creado Correctamente');
        return Redirect::to('/Lavado');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $vehiculo = Vehiculo::lists('placa','id');
        $usuario = 	User::lists('name','id');
        $lavados=	Lavado::find($id);
        //return view('encomienda.edit',['enco'=>$enco]);
        return view('lavado.edit',['lavados'=>$lavados],compact('usuario','vehiculo'));
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(LavadoUpdateRequest $request, $id)
	{
		$lavado =Lavado::find($id);
        $lavado->fill($request->all());
        $lavado->save();
        Session::flash('message','Lavado Actualizado Correctamente');
        return Redirect::to('/Lavado');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$lavado =Lavado::find($id);
        $lavado->delete();
        Session::flash('message','Lavado Eliminado Correctamente');
        return Redirect::to('/Lavado');

	}

}
