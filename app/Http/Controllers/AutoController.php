<?php namespace CarWash\Http\Controllers;

use CarWash\Http\Requests;
use CarWash\Http\Controllers\Controller;
use CarWash\Lavado;
use DB;
use Illuminate\Http\Request;

class AutoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$consulta = DB::select('SELECT v.propietario,v.placa,v.marca,v.color,count(l.id_vehiculo) as TOTAL FROM lavados l inner join vehiculos v on v.id=l.id_vehiculo GROUP BY id_vehiculo ORDER BY count(l.id_vehiculo) desc limit 1');	
        
        return view('mostrarAuto',['consulta'=>$consulta]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
