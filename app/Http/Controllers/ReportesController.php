<?php

namespace CarWash\Http\Controllers;

use Illuminate\Http\Request;
use CarWash\Lavado;
use CarWash\Usuario;
use CarWash\Vehiculo;
use CarWash\Comprobante;
use DB;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use CarWash\Http\Requests;
use CarWash\Http\Controllers\Controller;

class ReportesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	/*public function ingresos()
	{
			$ingresos = DB::select('SELECT sum(costototal) as GANANCIAS FROM comprobantes c INNER JOIN lavados l ON l.id = c.id_lavado WHERE c.fecha=:fecha',['fecha'=> $request->fecha]);
            return view('reportes.ingresos',['reporte'=>ingresos]);
	}*/
	
	public function autolavado()
	{
		$lavadas = DB::select('SELECT v.propietario,v.placa,v.marca,v.color,count(l.id_vehiculo) as TOTAL 
							   FROM lavados l inner join vehiculos v on v.id=l.id_vehiculo 
							   GROUP BY id_vehiculo ORDER BY count(l.id_vehiculo) desc limit 1');	
        
        return view('reportes.autolavado',['reporte'=>$lavadas]);
	}
	
	public function recordlavador()
	{
		 $recordlavador = DB::select('SELECT u.dni,u.estatus, u.name,u.apellidos, count(l.id_usuario) as TOTAL 
		 							FROM lavados l inner join users u on u.id=l.id_usuario 
		 							GROUP BY id_usuario ORDER BY count(l.id_usuario) desc limit 1');	
        
        return view('reportes.recordlavador',['reporte'=>$recordlavador]);
	}
	
	
	public function PDFautolavado(){
        $lavadas = DB::select('SELECT v.propietario,v.placa,v.marca,v.color,count(l.id_vehiculo) as TOTAL 
        					   FROM lavados l inner join vehiculos v on v.id=l.id_vehiculo 
        					   GROUP BY id_vehiculo ORDER BY count(l.id_vehiculo) desc limit 1');
        $pdf = PDF::loadView('reportes.pdf.pdfautolavado', ['record'=>$lavadas]);
        return $pdf->stream('reporte-autolavado.pdf');
    }

    public function PDFrecordlavador(){
        $recordlavador = DB::select('SELECT u.dni,u.estatus, u.name,u.apellidos, count(l.id_usuario) as TOTAL 
        							 FROM lavados l inner join users u on u.id=l.id_usuario GROUP BY id_usuario 
        							 ORDER BY count(l.id_usuario) desc limit 1');
        $pdf = PDF::loadView('reportes.pdf.pdfrecordlavador', ['record'=>$recordlavador]);
        return $pdf->stream('reporte-recordlavador.pdf');
    }
	
	
	
	public function Excelautolavado()
    {
        Excel::create('Reporte-Auto-Lavado', function($excel){
        $excel->sheet('Sheet', function($sheet){
            $lavadas = DB::select('SELECT v.propietario,v.placa,v.marca,v.color,count(l.id_vehiculo) as TOTAL 
            					   FROM lavados l inner join vehiculos v on v.id=l.id_vehiculo GROUP BY id_vehiculo 
            					   ORDER BY count(l.id_vehiculo) desc limit 1');
                $sheet->setFontFamily('Courier New');
                $sheet->setStyle(array(
                        'font' => array(
                            'size' =>  11,
                    )
                ));
            $sheet->loadView('reportes.excel.excelautolavado')->with('record', $lavadas);
        });
        })->export('xls');
    }


    public function Excelrecordlavador()
    {
        Excel::create('Reporte-Mejor-Lavador', function($excel){
        $excel->sheet('Sheet', function($sheet){
            $recordlavador = DB::select('SELECT u.dni,u.estatus, u.name,u.apellidos, count(l.id_usuario) as TOTAL 
            							 FROM lavados l inner join users u on u.id=l.id_usuario 
            							 GROUP BY id_usuario ORDER BY count(l.id_usuario) desc limit 1');
                    $sheet->setFontFamily('Courier New');
                    $sheet->setStyle(array(
                            'font' => array(
                                'size' =>  11,
                        )
                    ));
            $sheet->loadView('reportes.excel.excelrecordlavador')->with('record', $recordlavador);
        });
        })->export('xls');
    }
}
