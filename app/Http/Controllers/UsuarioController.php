<?php namespace CarWash\Http\Controllers;

use CarWash\Http\Requests;
use CarWash\Http\Requests\UserCreateRequest;
use CarWash\Http\Controllers\Controller;
use CarWash\User;
use Session;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use CarWash\Lavado;

class UsuarioController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function __construct(){
		$this->middleware('auth');
		$this->middleware('admin');
        $this->beforeFilter('@find',['only' => ['edit','update','destroy']]);
    }

    public function find(Route $route){
        $this->user = User::find($route->getParameter('Usuario'));
    }


	public function index()
	{
		$users = User:: paginate(3);
		return view('usuario.index',compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('usuario.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserCreateRequest $request)
	{
		    User::create([
		    'dni' => $request['dni'],
            'name' => $request['name'],
            'apellidos' => $request['apellidos'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            /*'password' => $request['password'],*/
            'estatus' => $request['estatus'],
            'tipo' => $request['tipo'],
        ]);
        Session::flash('message','Usuario Creado Correctamente');
        return Redirect::to('/Usuario');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return view('usuario.edit',['user'=>$this->user]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
        $this->user->fill($request->all());
        $this->user->save();
        Session::flash('message','Usuario Actualizado Correctamente');
        return Redirect::to('/Usuario');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->user->delete();
        Session::flash('message','Usuario Eliminado Correctamente');
        return Redirect::to('/Usuario');

	}

}
