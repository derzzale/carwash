<?php namespace CarWash\Http\Controllers;

use CarWash\Http\Requests;
use CarWash\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Agencia\Lavados;


class GananciaController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	$consulta = DB::select('SELECT sum(costototal) as GANANCIAS FROM comprobantes c INNER JOIN lavados l ON l.id = c.id_lavado WHERE c.fecha=:fecha',['fecha'=> $request->fecha]);

           
            if(empty($consulta)){ 
                return view('Busqueda');
            }
            else{
                return view('Busqueda',['consulta'=>$consulta]);
            }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$consulta = DB::select('SELECT c.id_usuario,sum(costototal) as GANANCIAS FROM comprobantes c INNER JOIN lavados l ON l.id = c.id_lavado WHERE c.fecha=:fecha',['fecha'=> $request->fecha]);

           
            if(empty($consulta)){
                echo '<script language="javascript">alert("EL Codigo no Existe");</script>'; 
                return view('Busqueda');
            }
            else{
                return view('Mostrar',['consulta'=>$consulta]);
            }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
