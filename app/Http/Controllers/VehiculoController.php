<?php namespace CarWash\Http\Controllers;

use CarWash\Http\Requests;
use CarWash\Http\Requests\VehiculoCreateRequest;
use CarWash\Http\Requests\VehiculoUpdateRequest;
use CarWash\Http\Controllers\Controller;
use CarWash\Vehiculo;
use CarWash\Lavado;
use Session;
use Redirect;
use Illuminate\Routing\Route;
use Illuminate\Http\Request;

class VehiculoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$vehiculos = Vehiculo::paginate(5);
          //envia infrmacion con compact
        return view('vehiculo.index',compact('vehiculos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('vehiculo.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(VehiculoCreateRequest $request)
	{
		Vehiculo::create([
			'propietario' => $request['propietario'],
            'placa' => $request['placa'],
            'marca' => $request['marca'],
            'modelo' => $request['modelo'],
            'color' => $request['color'],
         ]);
        return redirect('/Vehiculo')->with('message', 'Vehiculo Creado Correctamente');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$vehiculo =Vehiculo::find($id);
        return view('vehiculo.edit',['vehiculo'=>$vehiculo]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(VehiculoUpdateRequest $request, $id)
	{
		$vehiculo =Vehiculo::find($id);
        $vehiculo->fill($request->all());
        $vehiculo->save();
        Session::flash('message','Vehiculo Actualizado Correctamente');
        return Redirect::to('/Vehiculo');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$vehiculo =Vehiculo::find($id);
        $vehiculo->delete();
        Session::flash('message','Vehiculo Eliminado Correctamente');
        return Redirect::to('/Vehiculo');
	}

}
