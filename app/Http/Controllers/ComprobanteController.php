<?php namespace CarWash\Http\Controllers;

use CarWash\Http\Requests;
use CarWash\Http\Requests\ComprobanteCreateRequest;
use CarWash\Http\Requests\ComprobanteUpdateRequest;
use CarWash\Http\Controllers\Controller;
use CarWash\Comprobante;
use Session;
use Redirect;
use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use CarWash\Lavado;
Use CarWash\User;

class ComprobanteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$comprobantes = Comprobante::paginate(5);
          //envia infrmacion con compact
        return view('comprobante.index',compact('comprobantes'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
		$lavado = Lavado::lists('id','id');
        $usuario = User::lists('name','id');

        return view('comprobante.create',compact('lavado','usuario'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ComprobanteCreateRequest $request){
		    Comprobante::create([
            'nrocomprobante' => $request['nrocomprobante'],
            'tipocomprobante' => $request['tipocomprobante'],
            'hora' => $request['hora'],
            'fecha' => $request['fecha'],
            'costototal' => $request['costototal'],
            'id_usuario' => $request['id_usuario'],
            'id_lavado' => $request['id_lavado'],
        ]);
        Session::flash('message','Comprobante Creado Correctamente');
        return Redirect::to('/Comprobante');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$lavado = Lavado::lists('id','id');
        $usuario = 	User::lists('id','id');
        $comprobantes=	Comprobante::find($id);
        return view('comprobante.edit',['comprobantes'=>$comprobantes],compact('usuario','lavado'));
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ComprobanteUpdateRequest $request, $id)
	{
		$comprobante =Comprobante::find($id);
        $comprobante->fill($request->all());
        $comprobante->save();
        Session::flash('message','Comprobante Actualizado Correctamente');
        return Redirect::to('/Comprobante');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$comprobante =Comprobante::find($id);
        $comprobante->delete();
        Session::flash('message','Comprobante Eliminado Correctamente');
        return Redirect::to('/Comprobante');
	}
}
