<?php namespace CarWash\Http\Controllers;

use CarWash\Http\Requests;
use CarWash\Http\Controllers\Controller;

use Illuminate\Http\Request;

class FrontController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
  public function __construct(){
    $this->middleware('auth',['only' => 'admin']);
  }

  
   public function index(){
        return view('index');
   }

   public function contacto(){
        return view('contacto');
   }

   public function reviews(){
        return view('reviews');
   }




  public function admin(){
        return view('admin.index');
   }

      public function buscar(){
        return view('buscar');
   }


}
