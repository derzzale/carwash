<?php namespace CarWash\Http\Controllers;

use CarWash\Http\Requests;
use CarWash\Http\Requests\ClienteCreateRequest;
use CarWash\Http\Requests\ClienteUpdateRequest;
use CarWash\Http\Controllers\Controller;
use CarWash\Cliente;
use Session;
use Redirect;
use Illuminate\Routing\Route;
use Illuminate\Http\Request;

class ClienteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$clientes = Cliente::paginate(10);
          //envia infrmacion con compact 
        return view('cliente.index',compact('clientes'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('cliente.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ClienteCreateRequest $request)
	{
		Cliente::create([
            'dni' => $request['dni'],
            'nombres' => $request['nombres'],
            'apellidos' => $request['apellidos'],
            'sexo' => $request['sexo'],
            'telefono' => $request['telefono'],
         ]);
        return redirect('/Cliente')->with('message', 'Cliente Creado Correctamente');     
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//$cliente = Cliente::find($id);
		//return view('cliente.show',['cliente'=>$cliente]);
		//return view('cliente.show',['cliente'=>$cliente]);
		$cliente = Cliente::find($id);
        return View::make('cliente.show')->with('cliente', $cliente);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		  $cliente = Cliente::find($id);
          return view('clientes.edit',['cliente'=>$cliente]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ClienteUpdateRequest $request, $id)
	{
		$cliente =Cliente::find($id);
        $cliente->fill($request->all());
        $cliente->save();
        Session::flash('message','Usuario Actualizado Correctamente');
        return Redirect::to('/Cliente');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$cliente =Cliente::find($id);
        $cliente->delete();
        Session::flash('message','Usuario Eliminado Correctamente');
        return Redirect::to('/Cliente'); 
	}

}
