<?php namespace CarWash\Http\Controllers;

use CarWash\Http\Requests;
use CarWash\Http\Requests\UserCreateRequest;
use CarWash\Http\Controllers\Controller;
use CarWash\Lavado;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class MejorLavadorController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */




    
	public function index()
	{
	 $consulta = DB::select('SELECT u.dni,u.estatus, u.name,u.apellidos, count(l.id_usuario) as TOTAL FROM lavados l inner join users u on u.id=l.id_usuario GROUP BY id_usuario ORDER BY count(l.id_usuario) desc limit 1');	
        
        return view('mostrarLavador',['consulta'=>$consulta]);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{


	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		 //
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{


	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
	}

}
