<?php namespace CarWash;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model {

	public $table = "clientes";
	protected $fillable = ['dni', 'nombres', 'apellidos','sexo','telefono'];

}
