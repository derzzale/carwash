<?php namespace CarWash;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model {

	public $table = "vehiculos";
	protected $fillable = ['propietario','placa', 'marca', 'modelo','color'];


	//Relacion  con lavados
	public function lavados(){
			return $this->hasMany('CarWash\Lavado');
			//return $this->hasMany('Lavado::class');
	}
}
