<?php namespace CarWash;

use Illuminate\Database\Eloquent\Model;

class Comprobante extends Model {

	public $table = "comprobantes";
		/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable =  ['nrocomprobante','tipocomprobante','hora','fecha','costototal','id_usuario','id_lavado'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 protected $hidden =  ['nrocomprobante','tipocomprobante','hora','fecha','costototal','id_usuario','id_lavado'];


}
