<?php namespace CarWash;

use Illuminate\Database\Eloquent\Model;

class Lavado extends Model {

	protected $table = "lavados";
		/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['dni','tipo','costo','fechaingreso','fechasalida','culminacion','user_id','vehiculo_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	///protected $hidden = ['dni','tipo','costo','fechaingreso','fechasalida','culminacion','id_usuario','id_vehiculo'];

	// Relation with Usuario
	public function user()
	{
			return $this->belongsTo(User::class);
			//return $this->belongsTo('User::class');
	}
	//Realacion con Lavado
	public function vehiculo()
	{
			return $this->belongsTo(Vehiculo::class);
			//return $this->belongsTo('Vehiculo::class');
	}

}
